# OBJECT POOL #

An interface and default implementation for an asynchronous object pool.  The documentation is a work in progress right now.

## Contributing ##

Pull requests are encouraged.  We use [Git Flow](http://nvie.com/posts/a-successful-git-branching-model/) to manage release and development of the code.  Please make sure you are using the correct branch as a base for any pull requests.