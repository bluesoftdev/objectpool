/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrency;

import com.bluesoft.endurance.concurrent.ListenableFuture;

/**
 * Created by danap on 9/4/15.
 */
public class DefaultAsyncMutex extends DefaultAsyncSemaphore implements AsyncMutex<Permit> {
  public DefaultAsyncMutex() {
    super(1);
  }

  @Override
  public ListenableFuture<Permit> acquire() {
    return super.acquire();
  }

  @Override
  public void release(Permit permit) {
    super.release(permit);
  }
}
