package com.bluesoft.endurance.concurrency;

import com.bluesoft.endurance.concurrent.ListenableFuture;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by danap on 2/29/16.
 */
public class DefaultAsyncKeyedReadWriteLock<K> implements AsyncKeyedReadWriteLock<K> {
	private class LockHolder {
		AsyncReadWriteLock lock = new DefaultAsyncReadWriteLock();
		int accessorCount = 0;
		K key;

		LockHolder(K key) {
			this.key = key;
		}
	}

	private class KeyedPermit implements Permit {
		LockHolder holder;
		Permit permit;

		KeyedPermit(LockHolder holder, Permit permit) {
			this.holder = holder;
			this.permit = permit;
		}
	}

	private class KeyedReadPermit extends KeyedPermit implements Permit {
		KeyedReadPermit(LockHolder holder, Permit readPermit) {
			super(holder, readPermit);
		}
	}

	private class KeyedWritePermit extends KeyedPermit implements Permit {
		KeyedWritePermit(LockHolder holder, Permit writePermit) {
			super(holder, writePermit);
		}
	}

	private Map<K, LockHolder> locks = new HashMap<>();

	private synchronized LockHolder getOrCreateLockHolder(K key) {
		LockHolder holder = locks.get(key);
		if (holder == null) {
			holder = new LockHolder(key);
			locks.put(key, holder);
		}
		holder.accessorCount += 1;
		return holder;
	}

	private synchronized void releaseLockHolder(LockHolder holder) {
		holder.accessorCount -= 1;
		if (holder.accessorCount == 0) {
			locks.remove(holder.key);
		}
	}

	@Override
	public ListenableFuture<Permit> acquireReadLock(K key) {
		LockHolder holder = getOrCreateLockHolder(key);
		return holder.lock.acquireReadLock().map((p) -> new KeyedReadPermit(holder, p));
	}

	@Override
	public void releaseReadLock(Permit p) {
		KeyedReadPermit rp = (KeyedReadPermit) p;
		try {
			rp.holder.lock.releaseReadLock(rp.permit);
		} finally {
			releaseLockHolder(rp.holder);
		}
	}

	@Override
	public ListenableFuture<Permit> acquireWriteLock(K key) {
		LockHolder holder = getOrCreateLockHolder(key);
		return holder.lock.acquireWriteLock().map((p) -> new KeyedWritePermit(holder, p));
	}

	@Override
	public void releaseWriteLock(Permit p) {
		KeyedWritePermit wp = (KeyedWritePermit) p;
		try {
			wp.holder.lock.releaseReadLock(wp.permit);
		} finally {
			releaseLockHolder(wp.holder);
		}
	}
}

