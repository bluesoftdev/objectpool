/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrency;

import com.bluesoft.endurance.concurrent.ListenableFuture;

/**
 * A protocol for controlling access to a resource that is mutally exclusive.  Like in a threaded environment a mutex
 * can only be owned by one process at a time.  Unlike in a traditional monitor (e.g. {@link Object#wait()}) the thread
 * is not assigned ownership.  The caller is required to guarantee that the token will only be used by one thread at
 * a time.
 *
 * @param <T> the token type, any object type really.
 */
public interface AsyncMutex<T> {
  /**
   * Acquire the mutex.
   *
   * @return A token that can be used to release the mutex for another process to own.
   */
  ListenableFuture<T> acquire();

  /**
   * Releases the Mutex for another processes to own.
   *
   * @param permit the permit token that was returned from a previous call to {@link #acquire()}.
   */
  void release(T permit);
}
