package com.bluesoft.endurance.concurrency;

import com.bluesoft.endurance.concurrent.ListenableFuture;

/**
 * Provides read/write lock access semantics to a set of keyed read/write locks.  This allows finer grained access
 * control.
 *
 * Created by danap on 2/28/16.
 *
 * @param <K> The type of the key.
 */
public interface AsyncKeyedReadWriteLock<K> {

    ListenableFuture<Permit> acquireReadLock(K key);
    void releaseReadLock(Permit p);
    ListenableFuture<Permit> acquireWriteLock(K key);
    void releaseWriteLock(Permit p);
}
