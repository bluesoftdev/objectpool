/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrency;

import com.bluesoft.endurance.concurrent.ListenableFuture;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author danap
 */
public interface AsyncSemaphore<T extends Permit> {

  /**
   * Attempt to acquire the specified number of permits within the specified timeout.
   *
   * @param permits     the number of permits to acquire. Must be greater than 0.
   * @param timeout     the maximum time to wait
   * @param timeoutUnit the time unit for the timeout value.
   * @return A future that will return the number of permits acquired. Any number less then permits, indicates that the
   * process timed out before all the permits were acquired.  Even when the number returned is less then "permits" the
   * caller is responsible for releasing the permits acquired.
   */
  ListenableFuture<List<T>> acquire(int permits, long timeout, TimeUnit timeoutUnit);

  /**
   * Release the specified number of permits.
   *
   * @param permits the number of permits to release.
   */
  void release(List<T> permits);

  default ListenableFuture<List<T>> acquire(int permits) {
    return acquire(permits, -1L, TimeUnit.MILLISECONDS);
  }

  default ListenableFuture<T> acquire(long timeout, TimeUnit timeoutUnit) {
    return acquire(1, timeout, timeoutUnit).map(permits -> permits.get(0));
  }

  default ListenableFuture<T> acquire() {
    return acquire(1).map((List<T> permits) -> permits.get(0));
  }

  default void release(T permit) {
    release(Arrays.asList(permit));
  }
}
