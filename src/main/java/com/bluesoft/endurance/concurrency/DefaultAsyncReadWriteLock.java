package com.bluesoft.endurance.concurrency;

import com.bluesoft.endurance.concurrent.ListenableFuture;
import com.bluesoft.endurance.concurrent.SettableListenableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * An implementation of {@link AsyncReadWriteLock} that favors writers over readers.
 */
public class DefaultAsyncReadWriteLock implements AsyncReadWriteLock {
	private static final Logger LOG = LoggerFactory.getLogger(DefaultAsyncReadWriteLock.class);

	private class PermitRequest {
		public SettableListenableFuture<Permit> lockReciever;

		public PermitRequest(SettableListenableFuture<Permit> lockReciever) {
			this.lockReciever = lockReciever;
		}
	}

	private class ReadPermit implements Permit {
		boolean released = false;
		DefaultAsyncReadWriteLock getOwner() { return DefaultAsyncReadWriteLock.this; }
	}

	private class WritePermit implements Permit {
		boolean released = false;
		DefaultAsyncReadWriteLock getOwner() { return DefaultAsyncReadWriteLock.this; }
	}

	private Queue<PermitRequest> readRequests = new ConcurrentLinkedQueue<>();
	private Queue<PermitRequest> writeRequests = new ConcurrentLinkedQueue<>();
	private AtomicBoolean writerPermitGiven = new AtomicBoolean(false);
	private AtomicInteger readerPermitsGiven = new AtomicInteger(0);

	@Override
	public ListenableFuture<Permit> acquireReadLock() {
		SettableListenableFuture<Permit> readLock = new SettableListenableFuture<>();
		synchronized (this) {
			if (writerPermitGiven.get() || !writeRequests.isEmpty()) {
				if (!readRequests.offer(new PermitRequest(readLock))) {
					// TODO handle failed offer
				}
			} else {
				readerPermitsGiven.incrementAndGet();
				readLock.set(new ReadPermit());
			}
		}
		return readLock;
	}

	@Override
	public void releaseReadLock(Permit p) {
		ReadPermit rp = (ReadPermit) p;
		synchronized (rp) {
			if (rp.getOwner() != this) throw new IllegalArgumentException("that permit was not issued by this lock.");
			if (rp.released) throw new IllegalArgumentException("that permit has already been released.");
			rp.released = true;
		}
		List<Runnable> completionTasks = new LinkedList<>();
		synchronized (this) {
			if (readerPermitsGiven.decrementAndGet() == 0) {
				if (!writeRequests.isEmpty()) {
					if (writerPermitGiven.compareAndSet(false, true)) {
						PermitRequest req = writeRequests.poll();
						completionTasks.add(() -> req.lockReciever.set(new WritePermit()));
					}
				}
			}
		}
		completionTasks.forEach((r) -> r.run());
	}

	@Override
	public ListenableFuture<Permit> acquireWriteLock() {
		SettableListenableFuture<Permit> writeLock = new SettableListenableFuture<>();
		synchronized (this) {
			if (readerPermitsGiven.get() > 0 || !writerPermitGiven.compareAndSet(false, true)) {
				if (!writeRequests.offer(new PermitRequest(writeLock))) {
					// TODO: Handle failed offer, should never happen
				}
			} else {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Giving writer permit: readerPermitsGiven = {}, writerPermitGiven = {}", readerPermitsGiven.get(), writerPermitGiven.get());
				}
				writeLock.set(new WritePermit());
			}
		}
		return writeLock;
	}

	@Override
	public void releaseWriteLock(Permit p) {
		WritePermit wp = (WritePermit) p;
		synchronized (wp) {
			if (wp.getOwner() != this) throw new IllegalArgumentException("that permit was not issued by this lock.");
			if (wp.released) throw new IllegalArgumentException("that permit has already been released");
			wp.released = true;
		}
		List<Runnable> completionTasks = new LinkedList<>();
		synchronized (this) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Releasing writer permit: readerPermitsGiven = {}, writerPermitGiven = {}", readerPermitsGiven.get(), writerPermitGiven.get());
			}
			if (!writerPermitGiven.compareAndSet(true, false)) {
				throw new IllegalStateException("writer permit has not been given.");
			}
			if (!writeRequests.isEmpty()) {
				if (writerPermitGiven.compareAndSet(false,true)) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Giving writer permit: readerPermitsGiven = {}, writerPermitGiven = {}", readerPermitsGiven.get(), writerPermitGiven.get());
					}
					PermitRequest req = writeRequests.poll();
					completionTasks.add(() -> req.lockReciever.set(new WritePermit()));
				}
			} else {
				while (!readRequests.isEmpty()) {
					int readerCount = readerPermitsGiven.incrementAndGet();
					if (LOG.isDebugEnabled()) {
						LOG.debug("Giving reader permit: readerPermitsGiven = {}, writerPermitGiven = {}", readerCount, writerPermitGiven.get());
					}
					PermitRequest req = readRequests.poll();
					completionTasks.add(() -> req.lockReciever.set(new ReadPermit()));
				}
			}
		}
		completionTasks.forEach((r) -> r.run());
	}
}
