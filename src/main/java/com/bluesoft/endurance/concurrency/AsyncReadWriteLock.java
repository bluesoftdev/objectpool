package com.bluesoft.endurance.concurrency;

import com.bluesoft.endurance.concurrent.ListenableFuture;

/**
 * An asynchronous read/write lock.  An implementation should allow multiple readers at a time but only one writer.  The
 * implementation will be responsible for determining writer vs reader priority.  Here is an example usage:
 *
 * <pre>
 *   <code>
 * AsyncReadWriteLock lock = ...
 *
 * ListenableFuture&lt;Permit&gt; permit = lock.acquireReadLock();
 * permit.addListener(() -&gt; {
 *   try {
 *     // Do some reading logic
 *   } finally {
 *     lock.releaseReadLock(permit);
 *   }
 * })
 * ListenableFuture&lt;Permit&gt; writePermit = lock.acquireWriteLock();
 * writePermit.addListener(() -&gt; {
 *   try {
 *     // Do some writing logic.
 *   } finally {
 *     lock.releaseWriteLock(writePermit);
 *   }
 * })
 *   </code>
 * </pre>
 */
public interface AsyncReadWriteLock {

	/**
	 * Acquire a read lock.
	 * @return the permit that should be used when calling {@link #releaseReadLock(Permit)}
	 */
	ListenableFuture<Permit> acquireReadLock();

	/**
	 * Release a read lock.  May only be called once per read lock.
	 * @param p the permit that was acquired through {@link #acquireReadLock()}
	 * @throws IllegalArgumentException if the read lock has already been released.
	 */
	void releaseReadLock(Permit p);

	/**
	 * Acquire a write lock.
	 *
	 * @return the permit that should be used when calling {@link #releaseWriteLock(Permit)}.
	 */
	ListenableFuture<Permit> acquireWriteLock();

	/**
	 * Release a write lock.  May only be called once per write lock.
	 * @param p the permit that was acquired through {@link #acquireWriteLock()}.
	 * @throws IllegalArgumentException if the write lock has already been released.
	 */
	void releaseWriteLock(Permit p);
}
