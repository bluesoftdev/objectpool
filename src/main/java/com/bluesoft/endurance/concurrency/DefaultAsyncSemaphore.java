/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrency;

import com.bluesoft.endurance.concurrent.*;
import com.bluesoft.endurance.pool.AsynchronousObjectPool;
import com.bluesoft.endurance.pool.ObjectPool;
import com.bluesoft.endurance.pool.ResourceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Created by danap on 9/1/15.
 */
public class DefaultAsyncSemaphore implements AsyncSemaphore<Permit> {
  private static AtomicLong nextPermitId = new AtomicLong(1);

  static class DefaultPermit implements Permit {
    private long id = nextPermitId.getAndIncrement();

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      DefaultPermit that = (DefaultPermit) o;

      return id == that.id;

    }

    @Override
    public int hashCode() {
      return (int) (id ^ (id >>> 32));
    }
  }

  class PermitManager implements ResourceManager<Permit> {
    @Override
    public ListenableFuture<Permit> createNew() {
      return Futures.successful(new DefaultPermit());
    }

    @Override
    public void dispose(Permit resource) {
    }

    @Override
    public boolean check(Permit resource) {
      return true;
    }
  }

  private ObjectPool<Permit> holder;
  ListenableExecutorService executor;

  public DefaultAsyncSemaphore(int maxPermits, int maxWaitQueueDepth, ExecutorService executor) {
    holder = new AsynchronousObjectPool<>(maxPermits, maxPermits, maxPermits, Long.MAX_VALUE, Long.MAX_VALUE, maxWaitQueueDepth, new PermitManager(), executor);
    if (executor instanceof ListenableExecutorService) {
      this.executor = (ListenableExecutorService) executor;
    } else {
      this.executor = new ListenableExecutorServiceDecorator(executor);
    }
  }

  public DefaultAsyncSemaphore(int maxPermits) {
    this(maxPermits, -1, MoreExecutors.NON_BLOCKING);
  }

  @Override
  public ListenableFuture<List<Permit>> acquire(int permits, long timeout, TimeUnit timeoutUnit) {
    List<ListenableFuture<Permit>> permitFutures = new ArrayList<>(permits);
    if (permits == 0) {
      return Futures.successful(new ArrayList<>());
    }
    if (permits == 1) {
      return holder.leaseResource().map(Arrays::asList);
    }
    for (int i = 0; i < permits; i++) {
      permitFutures.add(holder.leaseResource(timeout, timeoutUnit));
    }
    return Futures.list(permitFutures, executor);
  }

  @Override
  public void release(List<Permit> permits) {
    permits.forEach(p -> holder.returnResource(p));
  }
}
