/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.pool;

import com.bluesoft.endurance.concurrent.ListenableFuture;

import java.util.concurrent.TimeUnit;

/**
 * A non-blocking ObjectPool.
 * <p>
 *
 * @param <R> the type of resource to be pooled.
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public interface ObjectPool<R> {

  ListenableFuture<R> leaseResource();

  ListenableFuture<R> leaseResource(long timeout, TimeUnit timeUnit);

  void returnResource(R resource);

  int getManagedResourceCount();

  int getIdleResourceCount();

  int getLeasedResourceCount();

  int getAcquiringResourceCount();

  int getPendingResourceCount();
}
