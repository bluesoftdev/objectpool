/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bluesoft.endurance.pool;

/**
 * @author danap
 */
public class TooManyPendingRequestsException extends RuntimeException {

  public TooManyPendingRequestsException() {
  }

  public TooManyPendingRequestsException(String message) {
    super(message);
  }

  public TooManyPendingRequestsException(String message, Throwable cause) {
    super(message, cause);
  }

  public TooManyPendingRequestsException(Throwable cause) {
    super(cause);
  }

}
