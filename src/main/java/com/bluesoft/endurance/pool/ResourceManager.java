/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.pool;

import com.bluesoft.endurance.concurrent.ListenableFuture;

/**
 * A interface for managing resources that are contained within a {@link ObjectPool}.
 * <p>
 *
 * @param <R> the type of the resource.
 *            <p>
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public interface ResourceManager<R> {

  /**
   * Create a new instance of the resource.
   * <p>
   *
   * @return a {@link ListenableFuture} that will contain the new resource.
   */
  ListenableFuture<R> createNew();

  /**
   * Dispose of the given resource instance.
   * <p>
   *
   * @param resource the resource to dispose of.
   */
  void dispose(R resource);

  /**
   * Check that a resource is in a usable state.
   * <p>
   *
   * @param resource the resource to check.
   *                 <p>
   * @return {@code true} if the resource is in a usable state, {@code false} otherwise.
   */
  boolean check(R resource);
}
