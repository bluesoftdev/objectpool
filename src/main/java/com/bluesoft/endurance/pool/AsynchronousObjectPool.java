/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.pool;

import com.bluesoft.endurance.concurrent.Futures;
import com.bluesoft.endurance.concurrent.ListenableFuture;
import com.bluesoft.endurance.concurrent.MoreExecutors;
import com.bluesoft.endurance.concurrent.SettableListenableFuture;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * An implementation of {@link ObjectPool} that is fully asynchronous.
 * <p>
 *
 * @param <R> The type of the resource that will be pooled.
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class AsynchronousObjectPool<R> implements ObjectPool<R> {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(AsynchronousObjectPool.class);

  private class Resource {

    private final long createTime;
    private long lastReturnTime;
    private Long lastLeasedTime;
    private final R resource;

    public Resource(R resource) {
      createTime = lastReturnTime = System.currentTimeMillis();
      lastLeasedTime = null;
      this.resource = resource;
    }

    public R getResource() {
      return resource;
    }

    public long getIdleTime() {
      return System.currentTimeMillis() - lastReturnTime;
    }

    public long getLifeTime() {
      return System.currentTimeMillis() - createTime;
    }

    public long getLeasedTime() {
      if (!isLeased()) {
        throw new IllegalStateException("resource is not leased");
      }
      return System.currentTimeMillis() - lastLeasedTime;
    }

    public boolean isLeased() {
      return lastLeasedTime != null && lastLeasedTime > lastReturnTime;
    }

    public void leased() {
      lastLeasedTime = System.currentTimeMillis();
    }

    public void returned() {
      lastReturnTime = System.currentTimeMillis();
    }

    @Override
    public int hashCode() {
      int hash = 5;
      hash = 79 * hash + Objects.hashCode(this.resource);
      return hash;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean equals(Object obj) {
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final Resource other = (Resource) obj;
      return Objects.equals(this.resource, other.resource);
    }
  }

  private class MonitorThread extends Thread {
    public MonitorThread() {
      super("AsynchronousObjectPool-MonitorThread");
      setDaemon(true);
    }

    @Override
    public void run() {
      try {
        while (true) {
          LOG.info("State:\ntotalResourceCount = {}\nacquiringResourceCount = {}\nleasedResourceCount = {}\n" +
                          "availableResourceCount = {}\npendingResourceCount = {}",
                  totalResourceCount, acquiringResourceCount, leasedResourceCount, availableResourceCount, pendingResourceCount);
          Thread.sleep(5000L);
        }
      } catch (InterruptedException ex) {
        // Time to quit...
      }
    }
  }

  // Config
  private final int maxWaitQueueDepth;
  private final int maxResources;
  private final int minIdle;
  private final int maxIdle;
  private final long maxIdleTime;
  private final long maxLifeTime;
  private final ResourceManager<R> resourceManager;
  private final Executor executor;
  private final boolean monitor = false;

  // State
  private final AtomicInteger totalResourceCount = new AtomicInteger(0);
  private final AtomicInteger acquiringResourceCount = new AtomicInteger(0);
  private final AtomicInteger leasedResourceCount = new AtomicInteger(0);
  private final AtomicInteger availableResourceCount = new AtomicInteger(0);
  private final AtomicInteger pendingResourceCount = new AtomicInteger(0);
  private final Queue<Resource> availableResources = new ConcurrentLinkedQueue<>();
  private final Map<R, Resource> leasedResources = new ConcurrentHashMap<>();
  private final Queue<SettableListenableFuture<R>> resourceRequests = new ConcurrentLinkedQueue<>();
  private final Timer timeoutTimer = MoreExecutors.TIMEOUT_TIMER;

  public AsynchronousObjectPool(int maxResources, int minIdle, int maxIdle, long maxIdleTime, long maxLifeTime,
                                int maxWaitQueueDepth, ResourceManager<R> resourceManager,
                                Executor executor) {
    if (maxResources < 1) {
      throw new IllegalArgumentException("max resources must be 1 or more");
    }
    if (minIdle < 0) {
      throw new IllegalArgumentException("min idle cannot be negative.");
    }
    if (maxIdle < 0) {
      throw new IllegalArgumentException("max idle cannot be negative.");
    }
    if (maxIdleTime < 0 || maxLifeTime < 0) {
      throw new IllegalArgumentException("the max times cannot be negative.");
    }
    if (maxWaitQueueDepth < -1) {
      throw new IllegalArgumentException("the max wait queue depth must either be -1, indicating no limit, 0 or a positive integer.");
    }
    if (resourceManager == null || executor == null) {
      throw new IllegalArgumentException("the resourceManager and executor are required parameters");
    }
    this.maxResources = maxResources;
    this.minIdle = minIdle;
    this.maxIdle = maxIdle;
    this.maxWaitQueueDepth = maxWaitQueueDepth;
    this.resourceManager = resourceManager;
    this.executor = executor;
    this.maxIdleTime = maxIdleTime;
    this.maxLifeTime = maxLifeTime;
    // attempt to create initial pool of elements.
    for (int i = 0; i < minIdle; i++) {
      acquireNewResourceIfPossible(null);
    }
    if (monitor) {
      new MonitorThread().start();
    }
  }

  public AsynchronousObjectPool(int maxResources, ResourceManager<R> resourceManager) {
    this(maxResources, 0, maxResources, Long.MAX_VALUE, Long.MAX_VALUE, -1, resourceManager, MoreExecutors.NON_BLOCKING);
  }

  @Override
  public ListenableFuture<R> leaseResource() {
    try {
      Resource r = null;
      while (r == null) {
        r = getAvailable();
        if (r == null) {
          break;
        }
        if (!check(r)) {
          discardResource(r);
          r = null;
        }
      }
      if (r == null) {
        final SettableListenableFuture<R> resourceRequest = new SettableListenableFuture<>();
        int pending = pendingResourceCount.incrementAndGet();
        if (maxWaitQueueDepth >= 0 && pending > maxWaitQueueDepth) {
          pendingResourceCount.decrementAndGet();
          throw new TooManyPendingRequestsException();
        }
        acquireNewResourceIfPossible(resourceRequest);
        resourceRequests.add(resourceRequest);
        resourceRequest.addListener(pendingResourceCount::decrementAndGet, executor);
        return resourceRequest;
      } else {
        lease(r);
        return Futures.successful(r.getResource());
      }
    } finally {
      if (availableResourceCount.get() < minIdle) {
        // try to acquire a new resource to replace the one we took.
        acquireNewResourceIfPossible(null);
      }
    }
  }

  @Override
  public ListenableFuture<R> leaseResource(long timeout, TimeUnit timeUnit) {
    final SettableListenableFuture<R> resourceRequest = (SettableListenableFuture<R>) leaseResource();
    if (timeout > 0) {
      // create the timeout exception so that the calling code is in the call stack.
      final TimeoutException timeoutException = new TimeoutException("timeout waiting for resource from object pool.");
      TimerTask timeoutTask = new TimerTask() {
        @Override
        public void run() {
          synchronized (resourceRequest) {
            if (!resourceRequest.isDone()) {
              resourceRequest.setException(timeoutException);
            }
          }
        }
      };
      timeoutTimer.schedule(timeoutTask, TimeUnit.MILLISECONDS.convert(timeout, timeUnit));
      resourceRequest.addListener(timeoutTask::cancel, executor);
    }
    return resourceRequest;
  }

  @Override
  public void returnResource(R resource) {
    Resource r = leasedResources.get(resource);
    if (r == null) {
      throw new ResourceNotLeasedException("attempt to return a resource that was not leased: " + resource);
    }
    if (r.getLifeTime() > maxLifeTime) {
      discardResource(r);
      if (pendingResourceCount.get() > 0) {
        acquireNewResourceIfPossible(null);
      }
      return;
    }
    unlease(r);
    while (true) {
      SettableListenableFuture<R> resourceRequest = resourceRequests.poll();
      if (resourceRequest == null) {
        break;
      }
      synchronized (resourceRequest) {
        if (!resourceRequest.isDone()) {
          lease(r);
          resourceRequest.set(resource);
          return;
        }
      }
    }
    makeAvailable(r);
  }

  private void discardResource(Resource r) {
    resourceManager.dispose(r.getResource());
    totalResourceCount.decrementAndGet();
  }

  /**
   * Acquires a new resource. Does not block. If a resourceRequest is provided, the new resource will be passed to it if
   * it has not already gotten a response.
   * <p>
   *
   * @param resourceRequest the resource request to send the new resource to, or null if there is none.
   */
  private void acquireNewResourceIfPossible(final SettableListenableFuture<R> resourceRequest) {
    int total = totalResourceCount.get();
    if (total < maxResources && totalResourceCount.compareAndSet(total, total + 1)) {
      acquiringResourceCount.incrementAndGet();
      ListenableFuture<R> newFuture = resourceManager.createNew();
      newFuture.addListener((ListenableFuture<R> future) -> {
        try {
          Resource newResource = new Resource(future.getUninterruptibly());
          if (resourceRequest != null) {
            synchronized (resourceRequest) {
              if (!resourceRequest.isDone()) {
                lease(newResource);
                resourceRequest.set(newResource.getResource());
              } else {
                makeAvailable(newResource);
              }
            }
          } else {
            makeAvailable(newResource);
          }
        } catch (ExecutionException ex) {
          if (resourceRequest != null) {
            synchronized (resourceRequest) {
              if (!resourceRequest.isDone()) {
                resourceRequest.setException(ex.getCause());
              }
            }
          } else {
            LOG.error("failed to acquire new resource", ex);
          }
        } finally {
          acquiringResourceCount.decrementAndGet();
        }
      }, executor);
    }
  }

  private boolean check(Resource r) {
    return r.getIdleTime() < maxIdleTime && resourceManager.check(r.getResource());
  }

  private Resource getAvailable() {
    Resource r = availableResources.poll();
    if (r != null) {
      availableResourceCount.decrementAndGet();
    }
    return r;
  }

  private void makeAvailable(Resource newResource) {
    int available = availableResourceCount.get();
    if (available < maxIdle && availableResourceCount.compareAndSet(available, available + 1)) {
      availableResources.add(newResource);
    } else {
      discardResource(newResource);
    }
  }

  private void lease(Resource r) {
    r.leased();
    leasedResources.put(r.getResource(), r);
    leasedResourceCount.incrementAndGet();
  }

  private void unlease(Resource r) {
    r.returned();
    leasedResources.remove(r.getResource());
    leasedResourceCount.decrementAndGet();
  }

  @Override
  public int getManagedResourceCount() {
    return totalResourceCount.get();
  }

  @Override
  public int getIdleResourceCount() {
    return availableResourceCount.get();
  }

  @Override
  public int getLeasedResourceCount() {
    return leasedResourceCount.get();
  }

  @Override
  public int getAcquiringResourceCount() {
    return acquiringResourceCount.get();
  }

  @Override
  public int getPendingResourceCount() {
    return pendingResourceCount.get();
  }
}
