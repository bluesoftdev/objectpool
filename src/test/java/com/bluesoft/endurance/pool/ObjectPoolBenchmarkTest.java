/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.pool;

import com.bluesoft.endurance.concurrent.Futures;
import com.bluesoft.endurance.concurrent.ListenableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Queue;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.LongAccumulator;

/**
 * @author danap
 */
public class ObjectPoolBenchmarkTest {

  private static final Logger LOG = LoggerFactory.getLogger(ObjectPoolBenchmarkTest.class);

  public static class TestResource {

    private int id;
    private String name;

    public TestResource(int id, String name) {
      this.id = id;
      this.name = name;
    }

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    @Override
    public int hashCode() {
      int hash = 5;
      hash = 83 * hash + this.id;
      return hash;
    }

    @Override
    public boolean equals(Object obj) {
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final TestResource other = (TestResource) obj;
      return this.id == other.id;
    }
  }

  public static class TestResourceManager implements ResourceManager<TestResource> {

    private final AtomicInteger id = new AtomicInteger(0);
    private final Queue<TestResource> open = new ConcurrentLinkedQueue<>();
    private final Queue<TestResource> disposed = new ConcurrentLinkedQueue<>();

    @Override
    public ListenableFuture<TestResource> createNew() {
      int newId = id.incrementAndGet();
      TestResource t = new TestResource(newId, "Test Resource - " + newId);
      open.offer(t);
      return Futures.successful(t);
    }

    @Override
    public void dispose(TestResource resource) {
      disposed.offer(resource);
      open.remove(resource);
    }

    @Override
    public boolean check(TestResource resource) {
      return open.contains(resource);
    }
  }

  private static final Object[][] _implementations = new Object[][]{
          new Object[]{new AsynchronousObjectPool<>(4, new TestResourceManager())}
  };

  @DataProvider
  public Object[][] implementations() {
    return _implementations;
  }

  private final Random rand = new Random();
  private final ScheduledExecutorService nonBlockingExecutor = Executors.newScheduledThreadPool(4);
  private final AtomicInteger requestedCount = new AtomicInteger(0);
  private final AtomicInteger acquiredCount = new AtomicInteger(0);
  private final AtomicInteger timeoutCount = new AtomicInteger(0);
  private final AtomicInteger backpressureCount = new AtomicInteger(0);
  private final AtomicInteger errorCount = new AtomicInteger(0);
  private final AtomicInteger concurrent = new AtomicInteger(0);
  private final LongAccumulator maxConcurrent = new LongAccumulator(Long::max, Long.MIN_VALUE);

  @Test(enabled = false,groups = "testObjectPool", threadPoolSize = 8, invocationCount = 500, dataProvider = "implementations")
  public void testObjectPool(ObjectPool<TestResource> pool) throws Exception {
    CountDownLatch doneLatch = new CountDownLatch(1);
    requestedCount.incrementAndGet();
    ListenableFuture<TestResource> future = pool.leaseResource();
    future.addListener((f) -> {
      try {
        int c = concurrent.incrementAndGet();
        maxConcurrent.accumulate(c);
        TestResource resource = f.getUninterruptibly();
        // LOG resource acquisition
        acquiredCount.incrementAndGet();
        long delay = Math.max(0, (long) (1000 + rand.nextGaussian() * 333));
        nonBlockingExecutor.schedule(() -> {
          try {
            pool.returnResource(resource);
          } finally {
            concurrent.decrementAndGet();
            doneLatch.countDown();
          }
        }, delay, TimeUnit.MILLISECONDS);
      } catch (ExecutionException ex) {
        if (ex.getCause() instanceof TimeoutException) {
          // LOG timeout
          timeoutCount.incrementAndGet();
        } else if (ex.getCause() instanceof TooManyPendingRequestsException) {
          backpressureCount.incrementAndGet();
        } else {
          // LOG Error
          errorCount.incrementAndGet();
        }
      }
    }, nonBlockingExecutor);
    doneLatch.await();
  }

  @AfterGroups(groups = "testObjectPool")
  public void afterTestObjectPool() {
    LOG.info("requestedCount = {}, acquiredCount = {}, timeoutCount = {}, backpressureCount = {}, errorCount = {}",
            requestedCount, acquiredCount, timeoutCount, backpressureCount, errorCount);
    LOG.info("maxConcurrent = {}", maxConcurrent);
    assert requestedCount.get() == acquiredCount.get() + timeoutCount.get() + backpressureCount.get();
    assert errorCount.get() == 0;
  }
}
