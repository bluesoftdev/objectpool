/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.pool;

import com.bluesoft.endurance.concurrent.Futures;
import com.bluesoft.endurance.concurrent.ListenableFuture;
import org.testng.annotations.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.testng.Assert.*;

/**
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class AsynchronousObjectPoolTest {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(AsynchronousObjectPoolTest.class);

  class TestResourceManger implements ResourceManager {
    @Override
    public ListenableFuture createNew() {
      return Futures.successful(new Object());
    }

    @Override
    public void dispose(Object resource) {
    }

    @Override
    public boolean check(Object resource) {
      return true;
    }
  }

  @Test
  public void testPool() throws Exception {
    ResourceManager mockRM = new TestResourceManger();
    ObjectPool pool = new AsynchronousObjectPool(3, mockRM);

    Object one = pool.leaseResource().getUninterruptibly();
    assertNotNull(one);
    assertEquals(pool.getIdleResourceCount(), 0);
    assertEquals(pool.getLeasedResourceCount(), 1);
    assertEquals(pool.getManagedResourceCount(), 1);
    //assertEquals(pool.getPendingResourceCount(), 0);

    Object two = pool.leaseResource().getUninterruptibly();
    assertNotNull(two);
    assertEquals(pool.getIdleResourceCount(), 0);
    assertEquals(pool.getLeasedResourceCount(), 2);
    assertEquals(pool.getManagedResourceCount(), 2);
    //assertEquals(pool.getPendingResourceCount(), 0);

    Object three = pool.leaseResource().getUninterruptibly();
    assertNotNull(three);
    assertEquals(pool.getIdleResourceCount(), 0);
    assertEquals(pool.getLeasedResourceCount(), 3);
    assertEquals(pool.getManagedResourceCount(), 3);
    //assertEquals(pool.getPendingResourceCount(), 0);

    ListenableFuture fourFuture = pool.leaseResource();
    assertFalse(fourFuture.isDone());
    assertFalse(fourFuture.isCancelled());
    assertEquals(pool.getIdleResourceCount(), 0);
    assertEquals(pool.getLeasedResourceCount(), 3);
    assertEquals(pool.getManagedResourceCount(), 3);
    //assertEquals(pool.getPendingResourceCount(), 1);

    pool.returnResource(three);

    Object four = fourFuture.getUninterruptibly();
    assertEquals(pool.getIdleResourceCount(), 0);
    assertEquals(pool.getLeasedResourceCount(), 3);
    assertEquals(pool.getManagedResourceCount(), 3);
    // assertEquals( pool.getPendingResourceCount(), 0 );
    assertSame(four, three);

    pool.returnResource(four);
    assertEquals(pool.getIdleResourceCount(), 1);
    assertEquals(pool.getLeasedResourceCount(), 2);
    assertEquals(pool.getManagedResourceCount(), 3);
    //assertEquals(pool.getPendingResourceCount(), 0);

    pool.returnResource(two);
    assertEquals(pool.getIdleResourceCount(), 2);
    assertEquals(pool.getLeasedResourceCount(), 1);
    assertEquals(pool.getManagedResourceCount(), 3);
    //assertEquals(pool.getPendingResourceCount(), 0);

    pool.returnResource(one);
    assertEquals(pool.getIdleResourceCount(), 3);
    assertEquals(pool.getLeasedResourceCount(), 0);
    assertEquals(pool.getManagedResourceCount(), 3);
    //assertEquals(pool.getPendingResourceCount(), 0);

    // Check what happens a resource is returned again.
    try {
      pool.returnResource(one);
      fail("expected a ResourceNotLeasedException");
    } catch (ResourceNotLeasedException ex) {
      // Got expected exception.
    }
  }

  @Test(timeOut = 200L)
  public void testTimeout() throws Exception {
    ResourceManager mockRM = new TestResourceManger();
    ObjectPool pool = new AsynchronousObjectPool(3, mockRM);

    Object one = pool.leaseResource().get();
    Object two = pool.leaseResource().get();
    Object three = pool.leaseResource().get();

    try {
      pool.leaseResource(1L, TimeUnit.MILLISECONDS).get();
      fail("Failed to get expected exception.");
    } catch (ExecutionException ex) {
      assertTrue(ex.getCause() instanceof TimeoutException);
    }

    ListenableFuture<Object> four = pool.leaseResource(10L, TimeUnit.MILLISECONDS);
    pool.returnResource(one);
    pool.returnResource(two);
    pool.returnResource(three);
    assertNotNull(four.get());
  }
}
