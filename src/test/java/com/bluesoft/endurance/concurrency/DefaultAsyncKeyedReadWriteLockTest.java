package com.bluesoft.endurance.concurrency;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by danap on 2/29/16.
 */
public class DefaultAsyncKeyedReadWriteLockTest {
	private static final Logger LOG = LoggerFactory.getLogger(DefaultAsyncKeyedReadWriteLockTest.class);
	public static final int READER_THREADS = 50;
	public static final int WRITER_THREADS = 10;
	private Random rand = new Random();

	@Test
	public void testKeyedReadWriteLock() throws Exception {
		AsyncKeyedReadWriteLock<Integer> lock = new DefaultAsyncKeyedReadWriteLock<>();
		Map<Integer,AtomicInteger> readLocks = new ConcurrentHashMap<>();
		Map<Integer,AtomicInteger> writeLocks = new ConcurrentHashMap<>();
		Map<Integer,AtomicInteger> maxReadLocks = new ConcurrentHashMap<>();
		Map<Integer,AtomicInteger> maxWriteLocks = new ConcurrentHashMap<>();
		AtomicInteger errors = new AtomicInteger(0);

		ExecutorService threadPool = Executors.newFixedThreadPool(READER_THREADS + WRITER_THREADS);

		List<Future<?>> readerThreads = IntStream.range(0, READER_THREADS).mapToObj(t -> threadPool.submit(() -> {
			try {
				LOG.info("READ {}: starting reader thread", t);
				while (true) {
					int key = rand.nextInt(5);
					Permit rp = lock.acquireReadLock(key).get();
					try {
						int rlCount = readLocks.computeIfAbsent(key,(k) -> new AtomicInteger(0)).incrementAndGet();
						int rlMax = maxReadLocks.computeIfAbsent(key,(k) -> new AtomicInteger(0)).accumulateAndGet(rlCount, Integer::max);
						LOG.info("READ {}: {} - readlock count = {}, readLockMax = {}", t, key, rlCount, rlMax);
						int wlCount = writeLocks.computeIfAbsent(key,(k) -> new AtomicInteger(0)).get();
						if (wlCount != 0) {
							LOG.error("READ {}: {} - Read lock granted at same time as write lock. writeLocks = {}", t, key, wlCount);
							errors.incrementAndGet();
						}
						Thread.sleep(100L + rand.nextInt(100));
					} finally {
						LOG.info("READ {}: {} - Releasing read lock.", t, key);
						readLocks.computeIfAbsent(key,(k) -> new AtomicInteger(0)).decrementAndGet();
						lock.releaseReadLock(rp);
					}
					Thread.sleep(10L + rand.nextInt(50));
				}
			} catch (ExecutionException ex) {
				LOG.error("execution exception", ex);
			} catch (InterruptedException ex) {
				LOG.error("interrupted", ex);
			}
		})).collect(Collectors.toList());

		List<Future<?>> writerThreads = IntStream.range(0, WRITER_THREADS).mapToObj(t -> threadPool.submit(() -> {
			try {
				LOG.info("WRITE {}: starting writer thread", t);
				while (true) {
					int key = rand.nextInt(5);
					Permit rp = lock.acquireWriteLock(key).get();
					try {
						int wlCount = writeLocks.computeIfAbsent(key,(k) -> new AtomicInteger(0)).incrementAndGet();
						if (wlCount > 1) {
							LOG.error("WRITE {}: {} - writeLock count is greater than 1: {}", t, key, wlCount);
							errors.incrementAndGet();
						}
						int wlMax = maxWriteLocks.computeIfAbsent(key,(k) -> new AtomicInteger(0)).accumulateAndGet(wlCount, Integer::max);
						LOG.info("WRITE {}: {} - writelock count = {}, writeLockMax = {}", t, key, wlCount, wlMax);
						int rlCount = readLocks.computeIfAbsent(key,(k) -> new AtomicInteger(0)).get();
						if (rlCount != 0) {
							LOG.error("WRITE {}: {} - Write lock granted at same time as read locks. readLocks = {}", t, key, rlCount);
							errors.incrementAndGet();
						}
						Thread.sleep(100L + rand.nextInt(100));
					} finally {
						LOG.info("WRITE {}: {} - releasing write lock", t, key);
						writeLocks.computeIfAbsent(key,(k) -> new AtomicInteger(0)).decrementAndGet();
						lock.releaseWriteLock(rp);
					}
					Thread.sleep(100L + rand.nextInt(500));
				}
			} catch (ExecutionException ex) {
				LOG.error("execution exception", ex);
			} catch (InterruptedException ex) {
				LOG.error("interrupted", ex);
			}
		})).collect(Collectors.toList());

		Thread.sleep(5000L);

		writerThreads.forEach(f -> f.cancel(true));
		readerThreads.forEach(f -> f.cancel(true));

		assert errors.get() == 0 : errors.get() + " errors encountered";
		maxReadLocks.forEach((k,v) -> {
			assert v.get() <= READER_THREADS: k + " - max read locks greater then number of reader threads!";
		});
		maxWriteLocks.forEach((k,v) -> {
			assert v.get() == 1 : k + " - Expected maxWriteLocks to be 1 but found " + v;
		});
	}
}
