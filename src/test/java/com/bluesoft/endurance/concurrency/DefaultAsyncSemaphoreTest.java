/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrency;

import com.bluesoft.endurance.concurrent.Futures;
import com.bluesoft.endurance.concurrent.ListenableFuture;
import com.bluesoft.endurance.concurrent.MoreExecutors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.LongAccumulator;

/**
 * Created by danap on 9/1/15.
 */
public class DefaultAsyncSemaphoreTest {

  private static final Logger LOG = LoggerFactory.getLogger(DefaultAsyncSemaphoreTest.class);


  @Test
  public void testSemaphore() throws Exception {
    AsyncSemaphore<Permit> semaphore = new DefaultAsyncSemaphore(5);
    AtomicInteger concurrent = new AtomicInteger(0);
    AtomicInteger acquired = new AtomicInteger(0);
    AtomicInteger released = new AtomicInteger(0);
    LongAccumulator maxConcurrent = new LongAccumulator(Long::max, Long.MIN_VALUE);
    try {
      List<ListenableFuture<Integer>> taskConcurrence = new ArrayList<>(20);
      for (int i = 0; i < 20; i++) {
        final int taskNum = i;
        LOG.info("acquiring permit for task {}", taskNum);
        taskConcurrence.add(semaphore.acquire(10, TimeUnit.SECONDS).flatMap(permit -> {
          acquired.incrementAndGet();
          int c = concurrent.incrementAndGet();
          maxConcurrent.accumulate(c);
          LOG.info("permit acquired for task {}, {} concurrent", taskNum, c);
          return MoreExecutors.SCHEDULED.schedule(() -> {
            LOG.info("task executed for task {}", taskNum);
            return "this is fun: " + c;
          }, 100, TimeUnit.MILLISECONDS).onCompletion(f -> {
            LOG.info("completing task {}", taskNum);
            LOG.info("decremented concurrent count {}, taskNum = {}", concurrent.decrementAndGet(), taskNum);
            semaphore.release(permit);
            released.incrementAndGet();
            LOG.info("permit released for task {}", taskNum);
          }).onException(ex -> LOG.error("For task {}", taskNum, ex))
                  .map(x -> c);
        }).onException(ex -> LOG.error("For task {}", taskNum, ex)));
      }
      List<Integer> concurrencies = Futures.list(taskConcurrence, MoreExecutors.NON_BLOCKING).get();
      LOG.info("concurrencies = {}", concurrencies);
      assert maxConcurrent.get() <= 5;
      assert concurrent.get() == 0;
    } finally {
      LOG.info("maxConcurrent = {}, total acquired = {}, total released = {}", maxConcurrent, acquired, released);
    }
  }
}
