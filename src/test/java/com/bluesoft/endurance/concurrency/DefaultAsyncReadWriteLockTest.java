package com.bluesoft.endurance.concurrency;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by danap on 2/28/16.
 */
public class DefaultAsyncReadWriteLockTest {
	private static final Logger LOG = LoggerFactory.getLogger(DefaultAsyncReadWriteLockTest.class);
	public static final int READER_THREADS = 10;
	public static final int WRITER_THREADS = 2;
	private Random rand = new Random();
	@Test
	public void testReadWriteLock() throws Exception {
		AsyncReadWriteLock lock = new DefaultAsyncReadWriteLock();
		AtomicInteger readLocks = new AtomicInteger(0);
		AtomicInteger writeLocks = new AtomicInteger(0);
		AtomicInteger maxReadLocks = new AtomicInteger(0);
		AtomicInteger maxWriteLocks = new AtomicInteger(0);
		AtomicInteger errors = new AtomicInteger(0);

		ExecutorService threadPool = Executors.newFixedThreadPool(READER_THREADS+WRITER_THREADS);

		List<Future<?>> readerThreads = IntStream.range(0,READER_THREADS).mapToObj(t -> threadPool.submit(() -> {
			try {
				LOG.info("READ {}: starting reader thread",t);
				while (true) {
					Permit rp = lock.acquireReadLock().get();
					try {
						int rlCount = readLocks.incrementAndGet();
						int rlMax = maxReadLocks.accumulateAndGet(rlCount,Integer::max);
						LOG.info("READ {}: readlock count = {}, readLockMax = {}",t,rlCount,rlMax);
						int wlCount = writeLocks.get();
						if ( wlCount != 0 ) {
							LOG.error("READ {}: Read lock granted at same time as write lock. writeLocks = {}",t,wlCount);
							errors.incrementAndGet();
						}
						Thread.sleep(100L+rand.nextInt(100));
					} finally {
						LOG.info("READ {}: Releasing read lock.",t);
						readLocks.decrementAndGet();
						lock.releaseReadLock(rp);
					}
					Thread.sleep(10L+rand.nextInt(50));
				}
			} catch(ExecutionException ex) {
				LOG.error("execution exception",ex);
			} catch(InterruptedException ex) {
				LOG.error("interrupted",ex);
			}
		})).collect(Collectors.toList());

		List<Future<?>> writerThreads = IntStream.range(0,WRITER_THREADS).mapToObj(t -> threadPool.submit(() -> {
			try {
				LOG.info("WRITE {}: starting writer thread",t);
				while (true) {
					Permit rp = lock.acquireWriteLock().get();
					try {
						int wlCount = writeLocks.incrementAndGet();
						if (wlCount > 1) {
							LOG.error("WRITE {}: writeLock count is greater than 1: {}",t,wlCount);
							errors.incrementAndGet();
						}
						int wlMax = maxWriteLocks.accumulateAndGet(wlCount,Integer::max);
						LOG.info("WRITE {}: writelock count = {}, writeLockMax = {}",t,wlCount,wlMax);
						int rlCount = readLocks.get();
						if ( rlCount != 0 ) {
							LOG.error("WRITE {}: Write lock granted at same time as read locks. readLocks = {}",t,rlCount);
							errors.incrementAndGet();
						}
						Thread.sleep(100L+rand.nextInt(100));
					} finally {
						LOG.info("WRITE {}: releasing write lock",t);
						writeLocks.decrementAndGet();
						lock.releaseWriteLock(rp);
					}
					Thread.sleep(100L+rand.nextInt(500));
				}
			} catch(ExecutionException ex) {
				LOG.error("execution exception",ex);
			} catch(InterruptedException ex) {
				LOG.error("interrupted",ex);
			}
		})).collect(Collectors.toList());

		Thread.sleep(5000L);

		writerThreads.forEach(f -> f.cancel(true));
		readerThreads.forEach(f -> f.cancel(true));

		assert errors.get() == 0: errors.get()+" errors encountered";
		assert maxReadLocks.get() <= READER_THREADS;
		assert maxWriteLocks.get() == 1 : "Expected maxWriteLocks to be 1 but found "+maxWriteLocks.get();
	}
}
